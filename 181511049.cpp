/* 	File 		: 181511049.cpp
*	Deskripsi 	: Body dari modul yang dikerjakan oleh Ivan
*	Dibuat Oleh	: Ivan Eka Putra - 181511049
*/

#include "header/181511049.h"

address h;
int lastkey_;

/*void init_array(int A[19][27], int level){
	int b,k;
	for(b=0; b<19; b++){
		for(k=0; k<27; k++){
			A[b][k]=MAP(level, b, k);
		}
	}
}*/

//Gameplay
// Modul untuk semua gameplay dijalankan
void game(){
	printf("\nESCAPE THE CASTLE -> BEGIN (Now Playing Level: 1)\n");
	
//VARIABEL LOKAL
	//GAMEPLAY VARIABEL
	MAP(&h);
	data rekap;
	int complete=0;
	int level=1;
	skor=0; 

	//PLAYER VARIABEL
	int lastkey=0;
	int life=3;
	int hposisi=1, vposisi=4;

	//ENEMY VARIABEL
	int idx;
	int nEnemy;
	int elastkey[5];
	int ehposisi[5], evposisi[5];
	int lootstealed[5];

//INPUT NAMA
	char name[10];
	setbkcolor(BLACK);
	cleardevice();
	settextjustify(LEFT_TEXT, TOP_TEXT);
	nameinput(441, 290, "YOUR NAME:              ", 220, name, COMPLEX_FONT, LEFT_TEXT, 3);
	cleardevice();

//GAMEPLAY
	while(!isGameOver(h, level, life, complete)){
		bool lastMoveSprite = GERAKKIRI5;
        bool lastLadderSprite = NAIKTANGGA1;
        bool lastMoveOnRopeSprite;
        bool playingFallMusic = false;
        char lastMoveMusic;
        char lastMoveOnRopeMusic;

        StopMusic();
		gameover=0;
		//Inisialisasi enemy setiap memulai suatu level(Jumlah Maksimal Enemy = 6)
		nEnemy=jumlah_enemy(level);
		for (idx=1; idx<=nEnemy; idx++){
			elastkey[idx]=3;
			ehposisi[idx]=1;
			evposisi[idx]=4;
			lootstealed[idx]=0;
		}
		posisi_enemy(level);

		//Mendefinisikan posisi karakter sesuai level
		posisi_karakter(level);

		//Menggambar Peta sesuai isi Array yang ditunjuk oleh pointer h
		show_map(h);

		//Menampilkan Objective
		ShowObjective(level);

		int loot=hitung_loot(h);//Variabel penyimpan jumlah loot yang tersedia dalam satu level
		int loot_e=0;			//Variabel penyimpan jumlah loot yang telah didapat
		int enemy_trapped=0;

		while(!isLevelUp(level, loot_e, enemy_trapped) && !isGameOver(h, level, life, complete)){
			idx=0;
			//Menampilkan Level yang sedang dimainkan
			sprintf(msg,"%d",level); outtextxy(1107,113,msg);

			//Jika tidak habis melakukan Drill, gambar karakter
			if (!isHabisDrill(lastkey)){
				draw_sprite(h, lastkey, hposisi, vposisi, &lastMoveSprite, &lastLadderSprite, &lastMoveMusic, &lastMoveOnRopeSprite, &lastMoveOnRopeMusic);
			}

			//ENEMY ACTION
			for (idx=1; idx<=nEnemy; idx++){
				EnemyStealLoot(h, idx, lootstealed);
				EnemyReturnLoot(h, idx, elastkey, evposisi, ehposisi, lootstealed, &enemy_trapped);
				draw_enemy(h, idx, elastkey, ehposisi, evposisi);
			}

			//PLAYER MOVEMENT & ACTION
            if(isJatuh(h)){
            	if(!playingFallMusic){
                    PlayMusic(FALL);
                    playingFallMusic = true;
            	}
            	moveDOWN(h, &lastkey, hposisi, &vposisi);
            	delay(50);
        	}else{
        	    if(playingFallMusic){
                    StopMusic();
                    playingFallMusic = false;
        	    }
                KeyboardInput(h, &lastkey, &hposisi, &vposisi, &lastMoveSprite, &lastLadderSprite, &lastMoveMusic, &lastMoveOnRopeSprite, &lastMoveOnRopeMusic);
        	}

        	//ENEMY MOVEMENT
        	EnemyAI(h, idx, elastkey, ehposisi, evposisi);
        	delay(10);
        	
        	//Melakukan perubahan nilai terhadap SKOR, NYAWA & HARTA yang telah didapat
			skor = hitung_skor(h);
			life=hitung_nyawa(h, &life, level, idx, nEnemy);
			loot_e=hitung_loote(h, &loot_e);
        	
        	//Menampilkan jumlah objective yang sudah terselesaikan
			IncrementObjectiveNumber(loot_e, enemy_trapped);
		}
		if(isLevelUp(level, loot_e, enemy_trapped)){
				LevelUp(&h, &level, &complete);
		}
	}
	GameOver(rekap, name, level, complete);
}

//Drill
// Modul untuk menggali balok
void Drill(void * arg){
    int baris, kolom;
    if(lastkey_ == 5){
        baris = i + 1;
        kolom = j - 1;
    } else {
        baris = i + 1;
        kolom = j + 1;
    }
    h->map[baris][kolom] = 7;
    readimagefile("gambar//Obj_Blank.jpg", 0+32*kolom, 0+32*baris, 32+32*kolom, 32+32*baris);
    delay(2000);
    h->map[baris][kolom] = 1;
    readimagefile("gambar//Obj_Hancur.jpg", 0+32*kolom, 0+32*baris, 32+32*kolom, 32+32*baris);
    delay(1000);
    readimagefile("gambar//Obj_Balok.jpg", 0+32*kolom, 0+32*baris, 32+32*kolom, 32+32*baris);
    _endthread();
}

// Modul untuk memeriksa apakah tombol yang ditekan adalah tombol yang digunakan untuk menggali atau bukan
bool isMauDrill(int key){
	if (key==65 || key==97 || key==83 || key==115){
		return true;
	} else {
		return false;
	}
}

//
void GetDrill(address h, int key, int *lastkey, int hposisi, int vposisi, bool *lastMoveSprite, bool *lastLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic){
	if((key == 65 || key == 97) && isTembok(h->map[i + 1][j - 1])){
            *lastkey = 5; lastkey_=*lastkey;
            draw_sprite(h, *lastkey, hposisi, vposisi, &(*lastMoveSprite), &(*lastLadderSprite), &(*lastMoveMusic), &(*lastMoveOnRopeSprite), &(*lastMoveOnRopeMusic));
            PlayMusic(DRILL);
            _beginthread(Drill, 0, NULL);
	}else if((key == 83 || key == 115) && isTembok(h->map[i + 1][j + 1])){
            *lastkey = 6; lastkey_=*lastkey;
            draw_sprite(h, *lastkey, hposisi, vposisi, &(*lastMoveSprite), &(*lastLadderSprite), &(*lastMoveMusic), &(*lastMoveOnRopeSprite), &(*lastMoveOnRopeMusic));
        	PlayMusic(DRILL);
        	_beginthread(Drill, 0, NULL);
            }
}

// Modul untuk memeriksa apakah player sehabis melakukan penggalian atau bukan
bool isHabisDrill(int p){
	if (p==5 || p==6){
		return true;
	} else {
		return false;
	}
}

//Game
// Modul untuk memeriksa apakah permainan sudah selesai atau belum
bool isGameCompleted(int complete){
	if (complete == 1){
		return true;
	} else {
		return false;
	}
}

// Modul untuk memeriksa apakah permainan game over atau belum
bool isGameOver(address h, int level, int life, int complete){
	if(life == 0 || isGameCompleted(complete)){
		gameover = 1;
		start_time=true;
		return true;
	} else {
		return false;
	}
}

// Modul untuk menampilkan halaman game over atau game complete
void GameOver(data rekap, char name[10], int level, int complete){
		printf("\nGAMEOVER");
		delay(2000);
		FILE *f_copy;
		f_copy=fopen("timer.dat","rt");
		timegame timer;
		fscanf(f_copy,"%d %d %d %d\n", &timer.jam, &timer.menit,&timer.detik, &timer.jumlah_waktu);
		while(!feof(f_copy))
		{
			fscanf(f_copy,"%d %d %d %d\n", &timer.jam, &timer.menit,&timer.detik, &timer.jumlah_waktu);
		}
		rekap.sumWaktu=timer.jumlah_waktu;
		rekap.waktu.detik=timer.detik;
		rekap.waktu.menit=timer.menit;
		rekap.waktu.jam=timer.jam;
		strcpy(rekap.nama,name);
		rekap.poin=skor;
		save_data(rekap);
		if(complete==1){
			readimagefile("gambar//GAME-WON.jpg", 413, 290, 953, 488);
		} else {
			readimagefile("gambar//GAME-OVER.jpg", 413, 290, 953, 488);
		}
		delay(4000);
		fclose(f_copy);
		cleardevice();
		main_menu();
}

// Modul untuk memeriksa apakah karakter terjebak di antara balok atau tidak
bool isTerjebak(address h){
	if((isBeton(h->map[i+1][j]) || isTembok(h->map[i+1][j])) && (isBeton(h->map[i][j+1]) || isTembok(h->map[i][j+1])) && (isBeton(h->map[i][j-1]) || isTembok(h->map[i][j-1])) && isLubang(h->map[i][j])){
		return true;
	} else {
		return false;
	}
}

//Objective
// Modul untuk memeriksa apakah tujuan setiap tingkat sudah tercapai atau belum
bool isObjectiveClear(int level, int loot_e, int enemy_trapped){
	switch(level){
		case 1:{
			if (loot_e>=4 && enemy_trapped>=1){
				return true;
			} else {
				return false;
			}
			break;
		}
		case 2:{
			if (loot_e>=4 && enemy_trapped>=2){
				return true;
			} else {
				return false;
			}
			break;
		}
		case 3:{
			if (loot_e>=6 && enemy_trapped>=1){
				return true;
			} else {
				return false;
			}
			break;
		}
		case 4:{
			if (loot_e>=6 && enemy_trapped>=2){
				return true;
			} else {
				return false;
			}
			break;
		}
		case 5:{
			if (loot_e>=8 && enemy_trapped>=3){
				return true;
			} else {
				return false;
			}
			break;
		}
		case 6:{
			if (loot_e>=8 && enemy_trapped>=3){
				return true;
			} else {
				return false;
			}
			break;
		}
	}
}

// Modul untuk menambah jumlah peti yang telah diambil pada antarmuka objective
void IncrementObjectiveNumber(int nChest, int enemy_trapped){
    // Increment chest objective number
    switch(nChest){
        case 1:
            outtextxy(1095, 463, "1");
            break;
        case 2:
            outtextxy(1095, 463, "2");
            break;
        case 3:
            outtextxy(1095, 463, "3");
            break;
        case 4:
            outtextxy(1095, 463, "4");
            break;
        case 5:
            outtextxy(1095, 463, "5");
            break;
        case 6:
            outtextxy(1095, 463, "6");
            break;
    }
    // Increment trap objective number
    switch(enemy_trapped){
        case 1:
            outtextxy(1095, 518, "1");
            break;
        case 2:
            outtextxy(1095, 518, "2");
            break;
        case 3:
            outtextxy(1095, 518, "3");
            break;
        case 4:
            outtextxy(1095, 518, "4");
            break;
        case 5:
            outtextxy(1095, 518, "5");
            break;
        case 6:
            outtextxy(1095, 518, "6");
            break;
    }
            
}

// Modul untuk menampilkan antarmuka objective
void ShowObjective(int level){
    // Display objective board image
    readimagefile("gambar//Objective-Board.jpg", 895, 0, 1335, 605);

    settextstyle(4, HORIZ_DIR, 2);

    // Display initial state objective number
    switch(level){
        case 1:
            outtextxy(1095, 463, "0/4");
            outtextxy(1095, 518, "0/1");
            break;
        case 2:
            outtextxy(1095, 463, "0/4");
            outtextxy(1095, 518, "0/2");
            break;
        case 3:
            outtextxy(1095, 463, "0/6");
            outtextxy(1095, 518, "0/1");
            break;
        case 4:
            outtextxy(1095, 463, "0/6");
            outtextxy(1095, 518, "0/2");
            break;
        case 5:
            outtextxy(1095, 463, "0/8");
            outtextxy(1095, 518, "0/3");
            break;
        case 6:
            outtextxy(1095, 463, "0/8");
            outtextxy(1095, 518, "0/3");
            break;
    }
}

// Audio
// Modul untuk memainkan musik pada permainan
void PlayMusic(int musicName){
    switch (musicName){
        case DRILL:
            PlaySound("wav\\Drill.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case SCORE:
            PlaySound("wav\\Chess.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case MOVE1:
            PlaySound("wav\\Move 1.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case MOVE2:
            PlaySound("wav\\Move 2.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case MOVEONLADDER1:
            PlaySound("wav\\Move on Ladder 1.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case MOVEONLADDER2:
            PlaySound("wav\\Move on Ladder 2.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case MOVEONROPE1:
            PlaySound("wav\\Move on Rope 1.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case MOVEONROPE2:
            PlaySound("wav\\Move on Rope 2.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case FALL:
            PlaySound("wav\\Fall.wav", NULL, SND_ASYNC | SND_FILENAME);
            break;
        case THEME:
            PlaySound("wav\\Theme Song.wav", NULL, SND_ASYNC | SND_FILENAME | SND_LOOP);
            break;
    }
}

// Modul untuk menghentikan musik jatuh saat player memijak balok setelah jatuh
void StopMusic(){
    PlaySound(NULL, NULL, 0);
}
