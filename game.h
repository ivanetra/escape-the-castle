/*
Deskripsi	: Header yang berisi seluruh header yang digunakan dalam project
*/

#ifndef GAME_H
#define GAME_H

//Header
#include <stdio.h>
#include <graphics.h>
#include <conio.h>
#include <stdlib.h>
#include <dos.h>
#include <time.h>
#include <process.h>
#include "variabel.h"
#include "header/181511005.h"
#include "header/181511013.h"
#include "header/181511015.h"
#include "header/181511019.h"
#include "header/181511049.h"

//IGNORE WARNING
#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma GCC diagnostic ignored "-Wconversion-null"

//DEFINE
#define maxwidth	1366
#define maxheight	768
#define USETREE 0
#define DRILL 1
#define SCORE 2
#define MOVE1 3
#define MOVE2 4
#define MOVEONLADDER1 5
#define MOVEONLADDER2 6
#define MOVEONROPE1 7
#define MOVEONROPE2 8
#define FALL 9
#define THEME 10
#define NAIKTANGGA1 true
#define NAIKTANGGA2 false
#define MENGGANTUNGKANAN2 false
#define MENGGANTUNGKANAN1 true
#define MENGGANTUNGKIRI2 false
#define MENGGANTUNGKIRI1 true
#define GERAKKANAN5 false
#define GERAKKANAN14 true
#define GERAKKIRI5 false
#define GERAKKIRI14 true
#define INSTRUCTION '1'
#define ABOUT '2'

#endif
