/* 	File 		: 181511005.h
*	Deskripsi 	: Header dari modul yang dikerjakan oleh Aji
*	Dibuat Oleh	: Aji Muhammad Zafar - 181511005
*/

#ifndef H_005
#define H_005
#include "..\game.h"

//AJI MUHAMMAD ZAFAR

//Movement
void KeyboardInput( address h, int *lastkey, int *hposisi, int *vposisi,  bool *lastMoveSprite, bool *lastLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic);
void moveUP(address h, int *lastkey, int hposisi, int *vposisi);
void moveDOWN(address h, int *lastkey, int hposisi, int *vposisi);
void moveRIGHT(address h, int *lastkey, int *hposisi, int vposisi);
void moveLEFT(address h, int *lastkey, int *hposisi, int vposisi);

//Scoring
int hitung_loot(address h);
int hitung_loote(address h, int *loot_e);
int hitung_skor(address h);

//Input Nama
void nameinput(int x, int y, char *title, int spacing, char name[10], int font, int direction, int charsize);
void bordernama();

//Leaderboard
void save_data(data player);
void urutkan();
void hiScoreLayout();
void waktu(void *argc);

#endif


