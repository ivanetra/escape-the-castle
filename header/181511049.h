/* 	File 		: 181511049.h
*	Deskripsi 	: Header dari modul yang dikerjakan oleh Ivan
*	Dibuat Oleh	: Ivan Eka Putra - 181511049
*/

#ifndef H_049
#define H_049
#include "..\game.h"

//IVAN EKA PUTRA

/*void init_array(int A[19][27], int level);*/

//Gameplay
// Modul untuk semua gameplay dijalankan
void game();

//Drill
// Modul untuk menggali balok
void Drill(void * arg);
// Modul untuk memeriksa apakah tombol yang ditekan adalah tombol yang digunakan untuk menggali atau bukan
bool isMauDrill(int key);
//
void GetDrill(address h, int key, int *lastkey, int hposisi, int vposisi, bool *lastMoveSprite, bool *lastMoveOnLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic);
// Modul untuk memeriksa apakah player sehabis melakukan penggalian atau bukan
bool isHabisDrill(int p);

//Game
// Modul untuk memeriksa apakah permainan sudah selesai atau belum
bool isGameCompleted(int complete);
// Modul untuk memeriksa apakah permainan game over atau belum
bool isGameOver(address h, int level, int life, int complete);
// Modul untuk menampilkan halaman game over atau game complete
void GameOver(data rekap, char name[10], int level, int complete);
// Modul untuk memeriksa apakah karakter terjebak di antara balok atau tidak
bool isTerjebak(address h);

//Objective
// Modul untuk memeriksa apakah tujuan setiap tingkat sudah tercapai atau belum
bool isObjectiveClear(int level, int loot_e, int enemy_trapped);
// Modul untuk menambah jumlah peti yang telah diambil dan jumlah menjebak enemy pada antarmuka objective
void IncrementObjectiveNumber(int nChest, int enemy_trapped);
// Modul untuk menampilkan antarmuka objective
void ShowObjective(int level);

// Audio
// Modul untuk memainkan musik pada permainan
void PlayMusic(int musicName);
// Modul untuk menghentikan musik jatuh saat player memijak balok setelah jatuh
void StopMusic();

#endif
