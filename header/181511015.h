/* 	File 		: 181511015.h
*	Deskripsi 	: Header dari modul yang dikerjakan oleh Hilmy
*	Dibuat Oleh	: Hilmy Oktoharitsa - 181511015
*/

#ifndef H_015
#define H_015
#include "..\game.h"
#pragma comment(lib, "winmm.lib")

void main_menu();
void klik (int sound);
void showAboutUs();
void music();
void walksound();
void laddersound();
void collectsound();
void drillsound();
void ropesound();
void fallsound();
void ShowInstruction();
#endif

