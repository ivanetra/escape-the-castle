/* 	File 		: 181511013.h
*	Deskripsi 	: Header dari modul yang dikerjakan oleh Febri
*	Dibuat Oleh	: Febriyoga Bagus Satria - 181511013
*/

#ifndef H_013
#define H_013
#include "..\game.h"

//FEBRI

//Player Movement Image
void choose_sprite(address h, int lastkey, int hposisi, int vposisi, bool *lastMoveSprite, bool *lastMoveOnLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic);
void draw_sprite(address h, int lastkey, int hposisi, int vposisi, bool *lastMoveSprite, bool *lastMoveOnLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic);
void isi_temp(address h, int lastkey, int hposisi, int vposisi);

//Enemy Movement
void choose_enemy(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
void draw_enemy(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
void isi_temp_enemy(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
void enemy_moveUP(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
void enemy_moveDOWN(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
void enemy_moveRIGHT(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
void enemy_moveLEFT(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);

//Enemy AI
void EnemyAI(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]);
addr_ptr Create_Node (elmt_list info);
void ins_node (addr_ptr *head, elmt_list elm);
elmt_list del_node (addr_ptr *head, elmt_list elm);
double heuristic(elmt_list start, elmt_list end);
elmt_list find_min_f (addr_ptr X);
elmt_list is_parent (addr_ptr closed_set, addr_map parent);
addr_ptr construct_path (addr_ptr closed_set, elmt_list start, elmt_list end);
addr_ptr find_neighbors (elmt_list cur, int map[19][27]);
bool is_in_list(addr_ptr head, elmt_list elm);
void generate_grid(int arr[19][27], address h);
addr_ptr a_star (elmt_list start, elmt_list end, int map[19][27]);

//Enemy Steal and Return Loot
void EnemyStealLoot(address h, int idx, int lootstealed[]);
void EnemyReturnLoot(address h, int idx, int elastkey[], int ehposisi[], int evposisi[], int lootstealed[], int *enemy_trapped);
bool isEnemyGetTrap(address h, int idx, int elastkey[], int ehposisi[], int evposisi[], int *enemy_trapped);

//Jatuh
bool isJatuh(address h);

#endif
