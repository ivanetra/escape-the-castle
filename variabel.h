/*
Deskripsi	: Tipe data bentukan dan variabel global yang digunakan
*/

#ifndef variabel_H_
#define variabel_H_

#define scr_length 1366
#define scr_width 768

typedef struct{
	short int jam, menit, detik, jumlah_waktu, x;
}timegame;

typedef struct{
	char nama[10];
	int poin;
	timegame waktu;
	int sumWaktu;
}data;

typedef struct saya *address;
typedef struct saya{
	int map[19][27];
	address next;
} naoncing;


typedef struct {
    int b,
        k;
} addr_map;

typedef struct{
    addr_map pos;
	addr_map parent;
    double f;
	double g;
}elmt_list;

typedef struct list_grid *addr_ptr;
struct list_grid{
    elmt_list info;
    addr_ptr next;
};

extern int i, j, ei[5], ej[5], skor, gameover;
extern bool start_time;
extern char msg[120];
extern bool sora;

#endif
