/* 	File 		: 181511005.cpp
*	Deskripsi 	: Body dari modul yang dikerjakan oleh Aji
*	Dibuat Oleh	: Aji Muhammad Zafar - 181511005
*/

#include "header/181511005.h"

void moveUP(address h, int *lastkey, int hposisi, int *vposisi){
	draw_object(h, i, j);
    *lastkey=1;
//	isi_temp(h, temp, *lastkey, hposisi, *vposisi);
	if ((isTangga(h->map[i][j])) && !isTembok(h->map[i-1][j]) && !isBeton(h->map[i-1][j])){
		(*vposisi)--;
		if (*vposisi<1){
			i--;
			*vposisi=4;
		}
	} else {
		*vposisi=*vposisi;
	}
}

void moveDOWN(address h, int *lastkey, int hposisi, int *vposisi){
	draw_object(h, i, j);
    *lastkey=2;
//	isi_temp(h, temp, *lastkey, hposisi, *vposisi);
	if (isTangga(h->map[i+1][j]) || isBlank(h->map[i+1][j]) || isTali(h->map[i+1][j]) || isHarta(h->map[i+1][j]) || isLubang(h->map[i+1][j])){
		(*vposisi)++;
		if (*vposisi>4){
			i++;
			*vposisi=1;
		}
	} else {
		*vposisi=*vposisi;
	}
}

void moveRIGHT(address h, int *lastkey, int *hposisi, int vposisi){
	draw_object(h, i, j);
    *lastkey=3;
//  isi_temp(h, temp, *lastkey, *hposisi, vposisi);
	if (((!isTembok(h->map[i][j+1]) && !isBeton(h->map[i][j+1])) || *hposisi<5) && (isBeton(h->map[i+1][j]) || isTembok(h->map[i+1][j]) || isTangga(h->map[i+1][j]) || isTali(h->map[i][j+1]) || isTali(h->map[i][j-1])) && j<26 ){
		(*hposisi)++;
		if (*hposisi>5){
			*hposisi=1;
			j++;
		}
	} else {
		*hposisi=*hposisi;
	}
}

void moveLEFT(address h, int *lastkey, int *hposisi, int vposisi){
	draw_object(h, i, j);
    *lastkey=4;
//	if (*hposisi==1){
//    	isi_temp(h, temp, *lastkey, *hposisi, vposisi);
//	}
	if (((!isTembok(h->map[i][j-1]) && !isBeton(h->map[i][j-1])) || *hposisi>1) && (isBeton(h->map[i+1][j]) || isTembok(h->map[i+1][j]) || isTangga(h->map[i+1][j]) || isTali(h->map[i][j+1]) || isTali(h->map[i][j-1])) && j>0 ){
		(*hposisi)--;
		if (*hposisi<1){
			*hposisi=5;
			j--;
		}
	} else {
		j=j;
	}
}

void KeyboardInput(address h, int *lastkey, int *hposisi, int *vposisi, bool *lastMoveSprite, bool *lastLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic){

    //KEY INPUT BERGERAK KE ATAS
    if (GetAsyncKeyState(VK_UP)){
    	delay(40);
    	moveUP(h, lastkey, *hposisi, vposisi);
	}
	//KEY INPUT BERGERAK KE BAWAH
	if (GetAsyncKeyState(VK_DOWN)){
		delay(40);
		moveDOWN(h, lastkey, *hposisi, vposisi);
	}
	//KEY INPUT BERGERAK KE KANAN
	if (GetAsyncKeyState(VK_RIGHT)){
		delay(40);
		moveRIGHT(h, lastkey, hposisi, *vposisi);
	}
	//KEY INPUT BERGERAK KE KIRI
	if (GetAsyncKeyState(VK_LEFT)){
		delay(40);
		moveLEFT(h, lastkey, hposisi, *vposisi);
	}
	//KEY INPUT DRILL KANAN
	if (GetAsyncKeyState(0x41)){
		int key=65;
    	GetDrill(h, key, lastkey, *hposisi, *vposisi, &(*lastMoveSprite), &(*lastLadderSprite), &(*lastMoveMusic), &(*lastMoveOnRopeSprite), &(*lastMoveOnRopeMusic));
	}
	//KEY INPUT DRILL KIRI
	if (GetAsyncKeyState(0x53)){
    	int key=83;
    	GetDrill(h, key, lastkey, *hposisi, *vposisi, &(*lastMoveSprite), &(*lastLadderSprite), &(*lastMoveMusic), &(*lastMoveOnRopeSprite), &(*lastMoveOnRopeMusic));
	}
}

int hitung_skor(address h)
{
	if(isHarta(h->map[i][j]))
	{
	    PlayMusic(SCORE);
		skor+=100;
	}
	if (skor==0){
		outtextxy(1082,240,"0000");
	} else if (skor >=100 && skor < 1000) {
		sprintf(msg,"%d",skor); outtextxy(1099,240,msg);
	} else {
		sprintf(msg,"%d",skor); outtextxy(1082,240,msg);
	}
	return skor;
}

int hitung_loote(address h, int *loot_e)
{
	if(isHarta(h->map[i][j]))
	{
		(*loot_e)++;
		h->map[i][j]=0;
	}
	return *loot_e;
}

int hitung_loot(address h)
{
	int loot = 0;
	int brs,klm;
	for(brs=0;brs<19;brs++)
	{
		for(klm=0;klm<27;klm++)
		{
			if(h->map[brs][klm]==5)
			{
				loot++;
			}
		}
	}
	return loot;
}

void bordernama()
{
	setcolor(WHITE);
	rectangle (414,264,830,334);
	setfillstyle(XHATCH_FILL,BLUE);
	floodfill(320,170,WHITE);
}


//input nama
void nameinput(int x, int y, char *title, int spacing, char name[10], int font, int direction, int charsize) {
	short int i;
	data rekap;
	bordernama();
	settextstyle(font, direction, charsize);
	outtextxy(x, y, title);
	for (i=0; i<10; i++) {
		name[i] = 0;
	}

	i = 0;
	while (i<10) {
		if (i<9) {
			do {
				name[i] = getch();
			} while (!(((name[i]>=97 && name[i]<=122) || (name[i]>=65 && name[i]<=90)) || (name[i]==8 || name[i]==13)));
			if (name[i]>=97 && name[i]<=122) {
				name[i] = name[i] - 32;
			}
		} else if (i==9) {
			do {
				name[i] = getch();
			} while (!(name[i]==8 || name[i]==13));
		}

		if (name[i]==8) {
			name[i] = 0;
			if (i>0) {
				name[i-1] = ' ';
				i--;
			}
		}
		if (name[i]==13) {
			name[i] = 0;
			if(name[0]!=0) {
				break;
			}
		}
		if ((name[i]>=65 && name[i]<=90)) {
			i++;
		}
		outtextxy(x+spacing, y, name);
	}
}


void urutkan()
{
	FILE *f_data;
	short int x=0, i;
	char print_rank[10], print_name[20], print_move[20], print_timer[20];
	f_data=fopen("leaderboard.dat","rt");
	data pengguna[10];
	data temp;
	int index=0;
	while(!feof(f_data))
	{
		fscanf(f_data,"%s %d %d %d %d %d\n", &pengguna[index].nama, &pengguna[index].waktu.jam,&pengguna[index].waktu.menit,&pengguna[index].waktu.detik,&pengguna[index].poin,&pengguna[index].sumWaktu);
		index++;
	}
	for(int x=0;x<index;x++)
	{
		for(int i =x+1;i<index;i++)
		{
			if(pengguna[x].poin<pengguna[i].poin)
			{
				temp=pengguna[x];
				pengguna[x]=pengguna[i];
				pengguna[i]=temp;
			}
			else if(pengguna[x].poin==pengguna[i].poin)
			{
				if(pengguna[x].sumWaktu>pengguna[i].sumWaktu)
				{
				temp=pengguna[x];
				pengguna[x]=pengguna[i];
				pengguna[i]=temp;
				}
			}

		}
	}


	x=0;
	for(int y=0; y<index;y++)
	{
		i = x; // i dipakai untuk nampung nilai x sekarang
		sprintf(print_rank, "%d", x+1); // sprintf nge reset value dari x
		x = i; // karena x ke reset, kembalikan nilai x yang tadi ditampung di i
		sprintf(print_name, "%s", pengguna[y].nama);
		sprintf(print_move, "%d", pengguna[y].poin);
		sprintf(print_timer, "%d:%02d:%02d", pengguna[y].waktu.jam, pengguna[y].waktu.menit, pengguna[y].waktu.detik);
		outtextxy(100,(130+(x*30)), print_rank);//
		outtextxy(250,130+(x*30), print_name);
		outtextxy(550,130+(x*30), print_timer);
		outtextxy(730,130+(x*30), print_move);
		x++;
	}
	fclose(f_data);
}

void hiScoreLayout() {
	int x, y;
	bool click=false;

	cleardevice();
	readimagefile("img\\BG2.bmp",0,0,840,620);
	rectangle(40, 40, 1326, 728);

	setfillstyle(SOLID_FILL, BLACK);

	floodfill(40 + ((800 - 40)/2), 40 + ((490 - 40)/2), WHITE);

	settextjustify(CENTER_TEXT, CENTER_TEXT);
	settextstyle(COMPLEX_FONT, HORIZ_DIR, 5);
	outtextxy(maxwidth/2, 40, "LEADERBOARDS");
	settextstyle(COMPLEX_FONT, HORIZ_DIR, 2);
	outtextxy(100,80, "RANK");
	outtextxy(250, 80, "NAME");
	outtextxy(550, 80, "WAKTU");
	outtextxy(730, 80, "POIN");

	urutkan();

	settextjustify(LEFT_TEXT, TOP_TEXT);
	do {
		readimagefile("gambar//Back.jpg",20,670,90,740);
		if (ismouseclick(WM_LBUTTONDOWN)) {
			getmouseclick(WM_LBUTTONDOWN, x, y);
			printf("%d %d ",x,y);
			if ((x>29 && x<101) && (y>649 && y<721)) {
				readimagefile("gambar//pushedback.jpg",20,670,90,740);
				delay(200);
				readimagefile("gambar//Back.jpg",20,670,90,740);
				click=true;
				delay(50);
				main_menu();
			} 
		}
	} while (click==false);
}

//SAVE TO FILE
void save_data(data player)
{
	cleardevice();
	FILE *f_pemain;

//	strcpy(player.nama,nama);
//	player.poin=x;
	// buka file & bikin file
	if((f_pemain = fopen("leaderboard.dat", "at+")) == NULL){
		printf("File tidak dapat dibuka!");
		exit(1);
	}

	//player.sumWaktu = player.waktu.s + (player.waktu.m * 60) + (player.waktu.H * 3600);
//	fscanf(f_pemain,"%s %d %d\n", &pengguna[index].nama, &pengguna[index].sumWaktu,&pengguna[index].poin);
//	fwrite(&player,sizeof(player),1,f_pemain);
	fprintf(f_pemain,"%s %d %d %d %d %d\n", player.nama, player.waktu.jam, player.waktu.menit, player.waktu.detik, player.poin,player.sumWaktu );
	//fprintf(f_pemain, "%s %d\n ", player.nama, player.poin);

	// tututp file
	fclose(f_pemain);
}

void waktu(void *argc)
{
	FILE *f_timer;
	f_timer=fopen("timer.dat","at+");
	char timer[20];
	timegame rekap;
	data siap;
	int ms;
	bool yes=true;
	rekap.jam=0;
	rekap.menit=0;
	rekap.detik=0;
	rekap.x=0;
	rekap.jumlah_waktu=0;
	while(yes)
	{
		sprintf(timer, "%d:%02d:%02d", rekap.jam, rekap.menit, rekap.detik);
		outtextxy(1067,50,timer);
		ms++;
		delay(100);
	if(ms == 10)
	{
		rekap.detik++;
		rekap.x++;
		ms = 0;
	}
	if(rekap.detik==60)
	{
		rekap.menit++;
		rekap.x++;
		rekap.detik = 0;
	}
	if(rekap.menit==60)
	{
		rekap.jam++;
		rekap.x++;
		rekap.menit = 0;
	}
	if(gameover==1)
	{
		yes=false;
	}

	}
	rekap.jumlah_waktu=(3600*rekap.jam)+(60*rekap.menit)+rekap.detik;
	fprintf(f_timer,"%d %d %d %d\n", rekap.jam, rekap.menit,rekap.detik, rekap.jumlah_waktu);
//	fwrite(&rekap,sizeof(rekap),1,f_timer);
	fclose(f_timer);
	_endthread();
}
