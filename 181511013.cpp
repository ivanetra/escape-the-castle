/* 	File 		: 181511013.cpp
*	Deskripsi 	: Body dari modul yang dikerjakan oleh Febri
*	Dibuat Oleh	: Febriyoga Bagus Satria - 181511013
*/

#include "header/181511013.h"

void choose_sprite(address h, int lastkey, int hposisi, int vposisi, bool *lastMoveSprite, bool *lastMoveOnLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic){
	if (lastkey==0){ //POSISI AWAL
		readimagefile("gambar//PLAYER//Blu.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
	}
	if (lastkey==1){ //Gambar yang akan dipilih setelah bergerak KE ATAS
		if (isTangga(h->map[i][j])){
			switch(vposisi){
				case 1 :{
					if(*lastMoveOnLadderSprite != NAIKTANGGA1){
                        PlayMusic(MOVEONLADDER1);
                   		*lastMoveOnLadderSprite = NAIKTANGGA1;
                    } else{
                        PlayMusic(MOVEONLADDER2);
                   		*lastMoveOnLadderSprite = NAIKTANGGA2;
                    }
					readimagefile("gambar//PLAYER//Naik_Tangga1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 2 :{
					readimagefile("gambar//PLAYER//Naik_Tangga2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 3 :{
					readimagefile("gambar//PLAYER//Naik_Tangga1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 4 :{
					readimagefile("gambar//PLAYER//Naik_Tangga2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
			}
		} else if(isTali(h->map[i][j])){
		    *lastMoveOnRopeSprite = MENGGANTUNGKANAN1;
			readimagefile("gambar//PLAYER//Menggantung_Kanan1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
		} else {
			readimagefile("gambar//PLAYER//Blu.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
		}
	}
	if (lastkey==2){
		if (isTangga(h->map[i][j])){
			switch(vposisi){
				case 1 :{
					if(*lastMoveOnLadderSprite != NAIKTANGGA1){
                        PlayMusic(MOVEONLADDER1);
                   		*lastMoveOnLadderSprite = NAIKTANGGA1;
                    } else{
                        PlayMusic(MOVEONLADDER2);
                   		*lastMoveOnLadderSprite = NAIKTANGGA2;
                    }
					readimagefile("gambar//PLAYER//Naik_Tangga1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 2 :{
					readimagefile("gambar//PLAYER//Naik_Tangga2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 3 :{
					readimagefile("gambar//PLAYER//Naik_Tangga1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 4 :{
					readimagefile("gambar//PLAYER//Naik_Tangga2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
			}
		} else if(isTali(h->map[i][j])){
		    *lastMoveOnRopeSprite = MENGGANTUNGKANAN1;
			readimagefile("gambar//PLAYER//Menggantung_Kanan1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
		} else {
			readimagefile("gambar//PLAYER//Blu.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
		}
	}
	if (lastkey==3){ //Key Terakhir = KEY_RIGHT
		if (isTali(h->map[i][j])){
			if (j%2==0){
                *lastMoveOnRopeSprite = MENGGANTUNGKANAN1;
				readimagefile("gambar//PLAYER//Menggantung_Kanan1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
			} else{
			    if(*lastMoveOnRopeMusic != MOVEONROPE1){
                    if(*lastMoveOnRopeSprite != MENGGANTUNGKANAN2){
                        PlayMusic(MOVEONROPE1);
                        *lastMoveOnRopeMusic = MOVEONROPE1;
                        *lastMoveOnRopeSprite = MENGGANTUNGKANAN2;
                    }
                } else{
                    if(*lastMoveOnRopeSprite != MENGGANTUNGKANAN2){
                        PlayMusic(MOVEONROPE2);
                        *lastMoveOnRopeMusic = MOVEONROPE2;
                        *lastMoveOnRopeSprite = MENGGANTUNGKANAN2;
                    }
                }
				readimagefile("gambar//PLAYER//Menggantung_Kanan2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
			}
		} else if (h->map[i][j+1]==1 && isTangga(h->map[i][j]) || isTangga(h->map[i][j])){
			if(*lastMoveOnLadderSprite != NAIKTANGGA1){
                PlayMusic(MOVEONLADDER1);
                *lastMoveOnLadderSprite = NAIKTANGGA1;
            }
			readimagefile("gambar//PLAYER//Naik_Tangga1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
		}else {
			switch(hposisi){
				case 1 : {
                        *lastMoveSprite = GERAKKANAN14;
						readimagefile("gambar//PLAYER//Gerak_Kanan1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 2 : {
                        *lastMoveSprite = GERAKKANAN14;
						readimagefile("gambar//PLAYER//Gerak_Kanan2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 3 : {
                        *lastMoveSprite = GERAKKANAN14;
						readimagefile("gambar//PLAYER//Gerak_Kanan3.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 4 : {
                        *lastMoveSprite = GERAKKANAN14;
						readimagefile("gambar//PLAYER//Gerak_Kanan4.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
				case 5 : {
                        if(*lastMoveMusic != MOVE1){
                            if(*lastMoveSprite != GERAKKANAN5){
                                PlayMusic(MOVE1);
                                *lastMoveMusic = MOVE1;
                                *lastMoveSprite = GERAKKANAN5;
                            }
                        } else{
                            if(*lastMoveSprite != GERAKKANAN5){
                                PlayMusic(MOVE2);
                                *lastMoveMusic = MOVE2;
                                *lastMoveSprite = GERAKKANAN5;
                            }
                        }
						readimagefile("gambar//PLAYER//Gerak_Kanan5.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
					break;
				}
			}
		}
	}
	if (lastkey==4){ //Key Terakhir = KEY_LEFT
		if (isTali(h->map[i][j])){
			if (j%2==0){
                *lastMoveOnRopeSprite = MENGGANTUNGKIRI1;
				readimagefile("gambar//PLAYER//Menggantung_Kiri1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
			} else{
			    if(*lastMoveOnRopeMusic != MOVEONROPE1){
                    if(*lastMoveOnRopeSprite != MENGGANTUNGKIRI2){
                        PlayMusic(MOVEONROPE1);
                        *lastMoveOnRopeMusic = MOVEONROPE1;
                        *lastMoveOnRopeSprite = MENGGANTUNGKIRI2;
                    }
                } else{
                    if(*lastMoveOnRopeSprite != MENGGANTUNGKIRI2){
                        PlayMusic(MOVEONROPE2);
                        *lastMoveOnRopeMusic = MOVEONROPE2;
                        *lastMoveOnRopeSprite = MENGGANTUNGKIRI2;
                    }
                }
				readimagefile("gambar//PLAYER//Menggantung_Kiri2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
			}
		} else if (h->map[i][j-1]==1 && isTangga(h->map[i][j])|| isTangga(h->map[i][j])){
			if(*lastMoveOnLadderSprite != NAIKTANGGA1){
                PlayMusic(MOVEONLADDER1);
                *lastMoveOnLadderSprite = NAIKTANGGA1;
            }
			readimagefile("gambar//PLAYER//Naik_Tangga1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
		}else {
				switch(hposisi){
					case 1 : {
					    *lastMoveSprite = GERAKKIRI14;
						readimagefile("gambar//PLAYER//Gerak_Kiri1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
						break;
					}
					case 2 : {
					    *lastMoveSprite = GERAKKIRI14;
						readimagefile("gambar//PLAYER//Gerak_Kiri2.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
						break;
					}
					case 3 : {
					    *lastMoveSprite = GERAKKIRI14;
						readimagefile("gambar//PLAYER//Gerak_Kiri3.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
						break;
					}
					case 4 : {
					    *lastMoveSprite = GERAKKIRI14;
						readimagefile("gambar//PLAYER//Gerak_Kiri4.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
						break;
					}
					case 5 : {
					    if(*lastMoveMusic != MOVE1){
                            if(*lastMoveSprite != GERAKKIRI5){
                                PlayMusic(MOVE1);
                                *lastMoveMusic = MOVE1;
                                *lastMoveSprite = GERAKKIRI5;
                            }
                        } else{
                            if(*lastMoveSprite != GERAKKIRI5){
                                PlayMusic(MOVE2);
                                *lastMoveMusic = MOVE2;
                                *lastMoveSprite = GERAKKIRI5;
                            }
                        }
						readimagefile("gambar//PLAYER//Gerak_Kiri5.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
						break;
					}
				}
		}
	}
	// Drills a hole on the left side of the lode runner
	if(lastkey==5){
        readimagefile("gambar//PLAYER//Gerak_Kiri1.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
	}
	// Drills a hole on the right side of the lode runner
	if(lastkey==6){
        readimagefile("gambar//PLAYER//Gerak_Kanan5.jpg", 0+32*j, 0+32*i, 32+32*j, 32+32*i);
	}
}

void draw_sprite(address h, int lastkey, int hposisi, int vposisi, bool *lastMoveSprite, bool *lastMoveOnLadderSprite, char *lastMoveMusic, bool *lastMoveOnRopeSprite, char *lastMoveOnRopeMusic){
//		h->map[i][j] = 4;
//		if (h->map[i][j]== 4){
			choose_sprite(h, lastkey, hposisi, vposisi, &(*lastMoveSprite), &(*lastMoveOnLadderSprite), &(*lastMoveMusic), &(*lastMoveOnRopeSprite), &(*lastMoveOnRopeMusic));
//		h->map[i][j]=temp;
//		}*/
}

/*void isi_temp(address h, int *temp, int lastkey, int hposisi, int vposisi){
	switch(lastkey){
		case 1 : {
			if((h->map[i+1][j]!=1 && h->map[i+1][j]!=3 && h->map[i-1][j]!=3 ) && !isTali(*temp) || (*temp==3 && !isBeton(h->map[i-1][j]) && !isTembok(h->map[i-1][j]) && vposisi==1)){
				*temp=h->map[i-1][j];
			} else {
				*temp=*temp;
			}
			break;
		}
		case 2 : {
			if(!isTembok(h->map[i+1][j]) || (isTali(h->map[i+1][j]) && vposisi==4)){
				*temp=h->map[i+1][j];
			} else {
				*temp=*temp;
			}
			break;
		}
		case 3 : {
			if(h->map[i][j+1]!=1 && h->map[i][j+1]!=5 && h->map[i][j+1]!=6 ){
				*temp=h->map[i][j+1];
			} else if(h->map[i][j+1]==5){
				*temp=0;
			} else {
				*temp=*temp;
			}
			break;
		}
		case 4 : {
			if(h->map[i][j-1]!=1 && h->map[i][j-1]!=5 && h->map[i][j-1]!=6 ){
				*temp=h->map[i][j-1];
			} else if(h->map[i][j-1]==5){
				*temp=0;
			} else {
				*temp=*temp;
			}
			break;
		}
	}
}*/

bool isJatuh(address h){
	if ((isBlank(h->map[i+1][j]) || isTali(h->map[i+1][j]) || isHarta(h->map[i+1][j]) || isLubang(h->map[i+1][j])) && !isTali(h->map[i][j]) && !isTangga(h->map[i][j])){
		return true;
	} else {
		return false;
	}
}

void draw_enemy(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
//	h->map[ei[idx]][ej[idx]]=9;
//	if (h->map[ei[idx]][ej[idx]]==9){
		choose_enemy(h, idx, elastkey, ehposisi, evposisi);
//		h->map[ei[idx]][ej[idx]]=etemp[idx];
//	}
}

void choose_enemy(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
	if (elastkey[idx]==0){ //POSISI AWAL
		readimagefile("gambar//ENEMY//bot.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
	}
	if (elastkey[idx]==1){ //Gambar yang akan dipilih setelah bergerak KE ATAS
		if (isTangga(h->map[ei[idx]][ej[idx]])){
			switch(evposisi[idx]){
				case 1 :{
					readimagefile("gambar//ENEMY//bottangga1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 2 :{
					readimagefile("gambar//ENEMY//bottangga2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 3 :{
					readimagefile("gambar//ENEMY//bottangga1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 4 :{
					readimagefile("gambar//ENEMY//bottangga2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
			}
		} else if(isTali(h->map[ei[idx]][ej[idx]])){
			readimagefile("gambar//ENEMY//botgantungkanan1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
		} else {
			readimagefile("gambar//ENEMY//bot.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
		}
	}
	if (elastkey[idx]==2){
		if (isTangga(h->map[ei[idx]][ej[idx]])){
			switch(evposisi[idx]){
				case 1 :{
					readimagefile("gambar//ENEMY//bottangga1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 2 :{
					readimagefile("gambar//ENEMY//bottangga2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 3 :{
					readimagefile("gambar//ENEMY//bottangga1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 4 :{
					readimagefile("gambar//ENEMY//bottangga2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
			}
		} else if(isTali(h->map[ei[idx]][ej[idx]])){
			readimagefile("gambar//ENEMY//botgantungkanan1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
		} else {
			readimagefile("gambar//ENEMY//bot.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
		}
	}
	if (elastkey[idx]==3){ //Key Terakhir = KEY_RIGHT
		if (isTali(h->map[ei[idx]][ej[idx]])){
			if (ej[idx]%2==0){
				readimagefile("gambar//ENEMY//botgantungkanan1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
			} else{
				readimagefile("gambar//ENEMY//botgantungkanan2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
			}
		} else if ((h->map[ei[idx]][ej[idx]+1]==1 && isTangga(h->map[ei[idx]][ej[idx]])) || isTangga(h->map[ei[idx]][ej[idx]])){
			readimagefile("gambar//ENEMY//bottangga1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
		}else {
			switch(ehposisi[idx]){
				case 1 : {
						readimagefile("gambar//ENEMY//bot_kanan1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 2 : {
						readimagefile("gambar//ENEMY//bot_kanan2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 3 : {
						readimagefile("gambar//ENEMY//bot_kanan3.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 4 : {
						readimagefile("gambar//ENEMY//bot_kanan4.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 5 :
				case 6 :
				case 7 : {
						readimagefile("gambar//ENEMY//bot_kanan5.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
			}
		}
	}
	if (elastkey[idx]==4){ //Key Terakhir = KEY_LEFT
		if (isTali(h->map[ei[idx]][ej[idx]])){
			if (ej[idx]%2==0){
				readimagefile("gambar//ENEMY//botgantungkiri1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
			} else{
				readimagefile("gambar//ENEMY//botgantungkiri2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
			}
		} else if ((h->map[ei[idx]][ej[idx]-1]==1 && isTangga(h->map[ei[idx]][ej[idx]]))|| isTangga(h->map[ei[idx]][ej[idx]])){
			readimagefile("gambar//ENEMY//bottangga1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
		} else {
			switch(ehposisi[idx]){
				case 1 : {
					readimagefile("gambar//ENEMY//bot_kiri5.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 2 : {
					readimagefile("gambar//ENEMY//bot_kiri4.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 3 : {
					readimagefile("gambar//ENEMY//bot_kiri3.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 4 : {
					readimagefile("gambar//ENEMY//bot_kiri2.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
				case 5 :
				case 6 :
				case 7 : {
					readimagefile("gambar//ENEMY//bot_kiri1.jpg", 0+32*ej[idx], 0+32*ei[idx], 32+32*ej[idx], 32+32*ei[idx]);
					break;
				}
			}
		}
	}
}

/*void isi_temp_enemy(address h, int idx, int etemp[], int elastkey[], int ehposisi[], int evposisi[]){
	switch(elastkey[idx]){
		case 1 : {
			if((h->map[ei[idx]+1][ej[idx]]!=1 && h->map[ei[idx]+1][ej[idx]]!=3 && h->map[ei[idx]-1][ej[idx]]!=3 ) && !isTali(etemp[idx]) || (etemp[idx]==3 && !isBeton(h->map[ei[idx]-1][ej[idx]]) && !isTembok(h->map[ei[idx]-1][ej[idx]]) && evposisi[idx]==1)){
				etemp[idx]=h->map[ei[idx]-1][ej[idx]];
			} else {
				etemp[idx]=etemp[idx];
			}
			break;
		}
		case 2 : {
			if(!isTembok(h->map[ei[idx]+1][ej[idx]]) || !isTali(h->map[ei[idx]+1][ej[idx]])){
				etemp[idx]=h->map[ei[idx]+1][ej[idx]];
			} else {
				etemp[idx]=etemp[idx];
			}
			break;
		}
		case 3 : {
			if(h->map[ei[idx]][ej[idx]+1]!=1 && h->map[ei[idx]][ej[idx]+1]!=5 && h->map[ei[idx]][ej[idx]+1]!=6 ){
				etemp[idx]=h->map[ei[idx]][ej[idx]+1];
			} else if(h->map[ei[idx]][ej[idx]+1]==5){
				etemp[idx]=0;
			} else {
				etemp[idx]=etemp[idx];
			}
			break;
		}
		case 4 : {
			if(h->map[ei[idx]][ej[idx]-1]!=1 && h->map[ei[idx]][ej[idx]-1]!=5 && h->map[ei[idx]][ej[idx]-1]!=6 ){
				etemp[idx]=h->map[ei[idx]][ej[idx]-1];
			} else if(h->map[ei[idx]][ej[idx]-1]==5){
				etemp[idx]=0;
			} else {
				etemp[idx]=etemp[idx];
			}
			break;
		}
	}
}*/

void enemy_moveUP(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
	draw_object(h, ei[idx], ej[idx]);
    elastkey[idx]=1;
//	isi_temp_enemy(h, idx, etemp, elastkey, ehposisi, evposisi);
	if ((isTangga(h->map[ei[idx]][ej[idx]])) && !isTembok(h->map[ei[idx]-1][ej[idx]]) && !isBeton(h->map[ei[idx]-1][ej[idx]])){
		(evposisi[idx])--;
		if (evposisi[idx]<1){
			ei[idx]--;
			evposisi[idx]=4;
		}
	} else {
		evposisi[idx]=evposisi[idx];
	}
}

void enemy_moveDOWN(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
	draw_object(h, ei[idx], ej[idx]);
    elastkey[idx]=2;
//	isi_temp_enemy(h, idx, etemp, elastkey, ehposisi, evposisi);
	if (isTangga(h->map[ei[idx]+1][ej[idx]]) || isBlank(h->map[ei[idx]+1][ej[idx]]) || isTali(h->map[ei[idx]+1][ej[idx]]) || isHarta(h->map[ei[idx]+1][ej[idx]]) || isLubang(h->map[ei[idx]+1][ej[idx]])){
		(evposisi[idx])++;
		if (evposisi[idx]>4){
			ei[idx]++;
			evposisi[idx]=1;
		}
	} else {
		evposisi[idx]=evposisi[idx];
	}
}

void enemy_moveRIGHT(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
	draw_object(h, ei[idx], ej[idx]);
    elastkey[idx]=3;
//  isi_temp_enemy(h, idx, etemp, elastkey, ehposisi, evposisi);
	if ((!isTembok(h->map[ei[idx]][ej[idx]+1]) && !isBeton(h->map[ei[idx]][ej[idx]+1])) && (isBeton(h->map[ei[idx]+1][ej[idx]]) || isTembok(h->map[ei[idx]+1][ej[idx]]) || isTangga(h->map[ei[idx]+1][ej[idx]]) || isTali(h->map[ei[idx]][ej[idx]+1]) || isTali(h->map[ei[idx]][ej[idx]-1])) && j<26){
		(ehposisi[idx])++;
		if (ehposisi[idx]>7){
			ehposisi[idx]=1;
			ej[idx]++;
		}
	} else {
		ehposisi[idx]=ehposisi[idx];
	}
}

void enemy_moveLEFT(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
	draw_object(h, ei[idx], ej[idx]);
    elastkey[idx]=4;
//  isi_temp_enemy(h, idx, etemp, elastkey, ehposisi, evposisi);
	if ((!isTembok(h->map[ei[idx]][ej[idx]-1]) && !isBeton(h->map[ei[idx]][ej[idx]-1])) && (isBeton(h->map[ei[idx]+1][ej[idx]]) || isTembok(h->map[ei[idx]+1][ej[idx]]) || isTangga(h->map[ei[idx]+1][ej[idx]]) || isTali(h->map[ei[idx]][ej[idx]+1]) || isTali(h->map[ei[idx]][ej[idx]-1])) && j>0){
		(ehposisi[idx])--;
		if (ehposisi[idx]<1){
			ehposisi[idx]=7;
			ej[idx]--;
		}
	} else {
		ej[idx]=ej[idx];
	}
}

void EnemyAI(address h, int idx, int elastkey[], int ehposisi[], int evposisi[]){
	//AI ENEMY 1
	if (ej[1] < j){
		enemy_moveRIGHT(h, 1, elastkey, ehposisi, evposisi);
	} else if (ej[1] > j){
		enemy_moveLEFT(h, 1, elastkey, ehposisi, evposisi);
	} else {
		ej[1] = ej[1];
	}
	
	//AI ENEMY 2
	if (isTembok(h->map[ei[2]][ej[2]-1]) || isBeton(h->map[ei[2]][ej[2]-1]) || isBlank(h->map[ei[2]+1][ej[2]-1])){
		elastkey[2]=3;
	} else if(isTembok(h->map[ei[2]][ej[2]+1]) || isBeton(h->map[ei[2]][ej[2]+1]) || isBlank(h->map[ei[2]+1][ej[2]+1])){
		elastkey[2]=4;
	}
	
	if (elastkey[2]==3){
		enemy_moveRIGHT(h, 2, elastkey, ehposisi, evposisi);
	} else if (elastkey[2]==4){
		enemy_moveLEFT(h, 2, elastkey, ehposisi, evposisi);
	}
	
	//AI ENEMY 3
	int grid[19][27];
	elmt_list start;
	elmt_list end;
    start.pos.b = ei[3]; 
	start.pos.k = ej[3];
    end.pos.b = i;
	end.pos.k = j;
	generate_grid(grid, h);
	addr_ptr path = a_star(start, end, grid);
	//printf("\nPATH CREATED\n");
	if (path != NULL){
		addr_ptr move = path->next;
		if (move->info.pos.k == ej[3] && move->info.pos.b < ei[3]){
			//printf("UP");
			elastkey[3]=1;
			enemy_moveUP(h, 3, elastkey, ehposisi, evposisi);
		} else if (move->info.pos.k == ej[3] && move->info.pos.b > ei[3]){
			//printf("DOWN");
			elastkey[3]=2;
			enemy_moveDOWN(h, 3, elastkey, ehposisi, evposisi);
		} else if (move->info.pos.k > ej[3] && move->info.pos.b == ei[3]){
			//printf("RIGHT");
			elastkey[3]=3;
			enemy_moveRIGHT(h, 3, elastkey, ehposisi, evposisi);
		} else if (move->info.pos.k < ej[3] && move->info.pos.b == ei[3]){
			//printf("LEFT");
			elastkey[3]=4;
			enemy_moveLEFT(h, 3, elastkey, ehposisi, evposisi);
		}
	} else {
		//printf("DIAM");
		elastkey[3]=1;
		ei[3]=ei[3]; ej[3]=ej[3];
	}
	
}

void EnemyStealLoot(address h, int idx, int lootstealed[])
{
	if(isHarta(h->map[ei[idx]][ej[idx]]))
	{
		h->map[ei[idx]][ej[idx]]=0;
		lootstealed[idx]++;
	}
}

void EnemyReturnLoot(address h, int idx, int elastkey[], int ehposisi[], int evposisi[], int lootstealed[], int *enemy_trapped)
{
	if(isEnemyGetTrap(h, idx, elastkey, evposisi, ehposisi, enemy_trapped))
	{
		if (lootstealed[idx]!=0){
			h->map[ei[idx]-1][ej[idx]]=5;
			draw_object(h, ei[idx]-1, ej[idx]);
			lootstealed[idx]--;
		}
	}
}

bool isEnemyGetTrap(address h, int idx, int elastkey[], int ehposisi[], int evposisi[], int *enemy_trapped){
	if (isLubang(h->map[ei[idx]+1][ej[idx]])){
		ei[idx] = ei[idx]+1;
		draw_enemy(h, idx, elastkey, ehposisi, evposisi);
		h->map[ei[idx]][ej[idx]]=1;
		(*enemy_trapped)++;
		return true;
	} else {
		return false;
	}
}

addr_ptr Create_Node (elmt_list info) {
	addr_ptr pNew;
	pNew = (addr_ptr) malloc(sizeof(list_grid));
	pNew->info = info;
	pNew->next = NULL;
	return pNew;
}

void ins_node (addr_ptr *head, elmt_list elm) {
	if(head == NULL){
	    *head = Create_Node(elm);
	}else{
	    addr_ptr pNew;
	    pNew = Create_Node(elm);
	    pNew->next = *head;
	    *head = pNew;
	}
}

elmt_list del_node (addr_ptr *head, elmt_list elm) {
	addr_ptr temp = *head; elmt_list X;
	while(temp != NULL){
	    if((elm.pos.b == temp->info.pos.b) && (elm.pos.k == temp->info.pos.k)){
			X = temp->info;
			if(*head == temp){
			    *head = temp->next;
			    temp->next = NULL;
			}else{
			    addr_ptr pBfr = *head;
			    while(pBfr->next != temp) pBfr = pBfr->next;
			    pBfr->next = temp->next;
			    temp->next = NULL;
			}
			free(temp);
			return X;
	    }
	    temp = temp->next;
	}
	return X;
}

double heuristic(elmt_list start, elmt_list end) {
	return abs(start.pos.k - end.pos.k) + abs(start.pos.b - end.pos.b);
}

elmt_list find_min_f (addr_ptr X) {
    addr_ptr temp = X;
    addr_ptr pMin = X;
    
    while(temp != NULL){
        if(temp->info.f < pMin->info.f){
            pMin = temp;
        }
        temp = temp->next;
    }
    return pMin->info;
}

elmt_list is_parent (addr_ptr closed_set, addr_map parent) {
	addr_ptr temp = closed_set;
	while (temp != NULL) {
		if (temp->info.pos.b == parent.b && temp->info.pos.k == parent.k) {
			return temp->info;
		}
		temp = temp->next;
	}
}

addr_ptr construct_path (addr_ptr closed_set, elmt_list start, elmt_list end) {
	addr_ptr path = NULL;
	ins_node (&path, end);
	while (end.pos.b != start.pos.b || end.pos.k != start.pos.k) {
		end = is_parent(closed_set,end.parent);
		ins_node (&path, end);

	}
	return path;
}

addr_ptr find_neighbors (elmt_list cur, int map[19][27]) {
	addr_ptr neighbors = NULL;
	elmt_list neighbor;
    int dirX[] = {0,0,-1,1};
    int dirY[] = {-1,1,0,0};
    
	for (int idx = 0; idx < 4; idx++) {
        neighbor.pos.b = cur.pos.b + dirY[idx];
        neighbor.pos.k = cur.pos.k + dirX[idx];
        if ((neighbor.pos.b >= 0) 
		&& (neighbor.pos.b < 19) 
		&& (neighbor.pos.k >= 0) 
		&& (neighbor.pos.k < 27) 
		&& map[neighbor.pos.b][neighbor.pos.k] == 0) {
        	neighbor.g = cur.g + 1.0;
        	neighbor.parent.b = cur.pos.b;
        	neighbor.parent.k = cur.pos.k;
			ins_node(&neighbors, neighbor);
		}
	}
	return neighbors;
}

bool is_in_list(addr_ptr head, elmt_list elm){
    addr_ptr temp = head;
    while(temp != NULL){
        if((temp->info.pos.b == elm.pos.b) && (temp->info.pos.k == elm.pos.k))
            if(temp->info.f >= elm.f)
                return true;
        temp = temp->next;
    }
    return false;
}

void generate_grid(int arr[19][27], address h){
    for(int brs = 0; brs < 19; brs++){
        for(int klm = 0; klm < 27; klm++){
            if((h->map[brs+1][klm]== 1 || h->map[brs+1][klm]== 7 || h->map[brs+1][klm]== 3) && h->map[brs][klm]== 0)
            {
            	arr[brs][klm] = false;
			}else if(h->map[brs][klm]== 3 || h->map[brs][klm]== 2 || h->map[brs][klm]== 5)
			{
				arr[brs][klm] = false;
			}else{
				arr[brs][klm] = true;
			}
        }
    }
}

addr_ptr a_star (elmt_list start, elmt_list end, int map[19][27]) {
	addr_ptr closed_set = NULL;
	addr_ptr open_set = NULL;
	addr_ptr neighbors;
	elmt_list cur, openneighbor;
	
	start.g = 0;
	start.f = start.g + heuristic(start, end);
	
	ins_node (&open_set, start);
	
	
	while (open_set != NULL) {
		cur = find_min_f (open_set);
		if(map[end.pos.b][end.pos.k]==1){
			return NULL;
		}
		//printf("[%d] [%d] [%d] [%d]\n", cur.pos.b, cur.pos.k, end.pos.b, end.pos.k);
		if (cur.pos.b == end.pos.b && cur.pos.k == end.pos.k) {
			end.parent = cur.parent;
			return construct_path(closed_set,start,end);
		}
		del_node (&open_set, cur);
		ins_node (&closed_set, cur);
		neighbors = find_neighbors(cur, map);
		while (neighbors != NULL) {
			if (!is_in_list(closed_set,neighbors->info)) {
				neighbors->info.f = neighbors->info.g + heuristic(neighbors->info, end);
				if (!is_in_list(open_set,neighbors->info)) {
					openneighbor = del_node (&neighbors, neighbors->info);
					ins_node (&open_set, openneighbor);
				}else {
					openneighbor = del_node (&open_set, neighbors->info);
					if (neighbors->info.g < openneighbor.g) {
						openneighbor.g = neighbors->info.g;
						openneighbor.parent = neighbors->info.parent;
						ins_node (&open_set, openneighbor);
					}
				}
			}else{
				del_node (&neighbors, neighbors->info);
			}
		}
	}
	return NULL;
}
