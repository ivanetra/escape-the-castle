/* 	File 		: 181511015.cpp
*	Deskripsi 	: Body dari modul yang dikerjakan oleh Hilmy
*	Dibuat Oleh	: Hilmy Oktoharitsa - 181511015
*/

#include "header/181511015.h"

void main_menu (){
	int sound =1;
	printf("\nMAIN MENU");
	readimagefile("gambar//mainmenu.bmp",0,0,1366,768);
	readimagefile("gambar//playup.jpg",520,378,830,472);
	readimagefile("gambar//aboutusup.jpg",1290,670,1354,734);
	if(sora==true)
	readimagefile("gambar//musicon.jpg",1220,678,1284,742);
	else
	readimagefile("gambar//musicoff.jpg",1220,678,1284,742);
	readimagefile("gambar//exit.jpg",1320,0,1360,40);
	readimagefile("gambar//score.jpg",70,670,134,734);
	readimagefile("gambar//howto.jpg",5,670,69,734);
	klik (sound);
}

void klik (int sound){
	int x=0, y=0;
	int play = 0;
	
	while (play == 0)
	{
	setbkcolor(BLACK);
	if(ismouseclick(WM_LBUTTONDOWN)){
			getmouseclick(WM_LBUTTONDOWN, x, y);
			printf("\nMouse Clicked at [%d] [%d] - ", x, y);
			if((x >= 519 && x <= 829) && (y >= 377 && y <= 471)){
				//GAMEPLAY
				readimagefile("gambar//playdown.jpg",520,378,830,472);
				delay(800);
				readimagefile("gambar//playup.jpg",520,378,830,472);
				delay(200);
				game();
				play = 1;
			}else if((x >= 69 && x <= 135) && (y >= 669 && y <= 735)){
				//HI-SCORE
				readimagefile("gambar//score.jpg",70,670,134,734);
				delay(200);
				hiScoreLayout();
				//tampil_leaderboard();
				play = 1;
			}else if((x >= 1289 && x<=1353) && (y >= 669 && y <= 733)){
				//Developer (About Us)
				readimagefile ("gambar//aboutusup.jpg",1290,670,1354,734);
				delay (200);
				showAboutUs();
			}else if((x >= 4 && x <= 70) && (y >= 669 && y <= 735)){
				//How to Play
				readimagefile("gambar//howto.jpg",5,670,69,734);
				delay(200);
				ShowInstruction ();
			}else if((x >= 1219 && x <= 1283) && (y >= 677 && y <= 743)){
				if (sound == 0){
					sound =1;
					sora=true;
					readimagefile("gambar//musicon.jpg",1220,678,1284,742);
					PlayMusic(THEME);
				} else {
					sound=0;
					sora=false;
					readimagefile("gambar//musicoff.jpg",1220,678,1284,742);
					StopMusic();
				}
				klik(sound);
			}else if((x >= 1319 && x <= 1379) && (y >= 0 && y <= 39)){
				exit (0);
			}
		}else play = 0;
	}
	cleardevice();
}

void showAboutUs(){
	int x, y;
	bool click=false;
	
	cleardevice();
	setfillstyle(SOLID_FILL, BLACK);
	floodfill(40 + ((800 - 40)/2), 40 + ((490 - 40)/2), WHITE);
	readimagefile ("gambar//About-Page.bmp",0,0,1366,768);
	do {
		setbkcolor(BLACK);
		readimagefile("gambar//Back.jpg",30,650,100,720);
		//outtextxy(1175,700,"-Under Development 2019-");
		if (ismouseclick(WM_LBUTTONDOWN)) {
			getmouseclick(WM_LBUTTONDOWN, x, y);
			printf("%d %d ",x,y);
			if ((x>29 && x<101) && (y>649 && y<721)) {
				readimagefile("gambar//pushedback.jpg",30,650,100,720);
				delay(200);
				readimagefile("gambar//Back.jpg",30,650,100,720);
				click=true;
				delay(50);
				main_menu();
			} 
		}
	} while (click==false);
}


void ShowInstruction(){
	int x, y;
	bool click=false;
	
	cleardevice();
	readimagefile ("gambar//Instruction Page.bmp",0,0,1366,768);
	do {
		setbkcolor(BLACK);
		readimagefile("gambar//Back.jpg",30,650,100,720);
		if (ismouseclick(WM_LBUTTONDOWN)) {
			getmouseclick(WM_LBUTTONDOWN, x, y);
			printf("%d %d ",x,y);
			if ((x>29 && x<101) && (y>649 && y<721)) {
				readimagefile("gambar//pushedback.jpg",30,650,100,720);
				delay(200);
				readimagefile("gambar//Back.jpg",30,650,100,720);
				click=true;
				delay(50);
				main_menu();
			} 
		}
	} while (click==false);
}
